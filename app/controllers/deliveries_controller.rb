# frozen_string_literal: true

class DeliveriesController < ApplicationController
  before_action :set_geocoder

  def create
    unless @current_user
      flash[:info] = 'Vous devez être connecté pour accéder à cette page!'
      session[:redirected_to] = 'createOffer'
      redirect_to '/users/login'
    end
  end

  def checkCreate
    address = params[:address]
    results = @geocoder.geocode(address)
    preciseAddress = results.first.address
    latitude = results.first.coordinates[0]
    longitude = results.first.coordinates[1]
    endV = params[:start_day] + params[:end_time]
    startDate = DateTime.parse(params[:start_day] + ' ' + params[:start_time])
    endDate = DateTime.parse(params[:start_day] + ' ' + params[:end_time])

    newOffer = Offer.new(start: startDate, end: endDate, address: preciseAddress, description: params[:description], user_id: session[:user_id], latitude: latitude, longitude: longitude)
    if startDate < endDate && newOffer.save
      flash[:info] = 'Votre offre a été créé avec succès! '
      notifyNeighbours(newOffer)
      redirect_to '/'
    else
      if startDate > endDate
        flash[:info] = "Échec de la création de l'offre, la date de départ doit être inférieure à celle d'arrivée!"
      else
        flash[:info] = "Échec de la création de l'offre"
      end
      redirect_to '/deliveries/create'
    end
  end

  def search
    adress = params[:adress]
    @results = @geocoder.geocode(adress)
    @lat = @results.first.coordinates[0]
    @lng = @results.first.coordinates[1]
    allOffers = Offer.all
    polishedOffers = []

    allOffers.each do |the_offer|
      ownOffer = false
      blocked = false
      if @current_user
        blocked = Blocked.where(blocked_by: the_offer.user_id, blocked_user: @current_user.id).exists?
        ownOffer = true if @current_user.id == the_offer.user_id
      end

      next unless haversineDistance(@lat, @lng, the_offer.latitude, the_offer.longitude) < 1 # <= distance en Km

      polishedOffers << the_offer if the_offer.start >= DateTime.now && !ownOffer && !blocked
    end
    @offers = polishedOffers
  end

  def contact
    unless @current_user
      flash[:info] = 'Vous devez être connecté pour accéder à cette page!'
      redirect_to '/users/login'
    end
    chat = Chat.where(id: params[:id]).first
    livreurId = Offer.where(id: chat.offer_id).first.user_id
    voisinId = chat.user_id
    @blocked = Blocked.where(blocked_by: voisinId, blocked_user: livreurId).exists?
    @livreur = User.where(id: livreurId).first
    @voisin = User.where(id: voisinId).first
    @messages = Message.where(chat_id: params[:id])
  end

  def createChat
    if @current_user
      chatExist = Chat.where(offer_id: params[:id], user_id: @current_user.id).first
      if chatExist
        redirect_to "/deliveries/contact/#{chatExist.id}"
      else
        newChat = Chat.new(offer_id: params[:id], user_id: @current_user.id)
        if newChat.save
          redirect_to "/deliveries/contact/#{newChat.id}"
        else
          flash[:info] = 'Erreur!'
          redirect_to '/deliveries/search'
        end
      end

    else
      flash[:info] = 'Vous devez être connecté pour effectuer cette action!'
      session[:redirected_to] = 'contact'
      session[:chatId] = params[:id]
      redirect_to '/users/login'
    end
  end

  def sendMessage
    content = params[:content]
    sender_id = @current_user.id
    chat_id = params[:chatId]
    newMessage = Message.new(content: params[:content], timestamp: Time.now.getutc, sender_id: sender_id, chat_id: chat_id)
    if newMessage.save
      flash[:info] = 'Message envoyé!'
      redirect_to "/deliveries/contact/#{chat_id}"
    else
      flash[:info] = "Échec de l'envoi"
      redirect_to "/deliveries/contact/#{chat_id}"
    end
  end

  def myOffers
    unless @current_user
      flash[:info] = 'Vous devez être connecté pour accéder à cette page!'
      redirect_to '/users/login'
    end
    user = @current_user
    @offers = Offer.where(user_id: user.id)
    @chats = Chat.where(user_id: user.id)
  end

  private

  def set_geocoder
    require 'opencage/geocoder'
    @geocoder = OpenCage::Geocoder.new(api_key: '89f4b6ebed4249c5b74547cfca1ab2b0')
  end

  def haversineDistance(lat1, lon1, lat2, lon2)
    earthRadiusKm = 6371

    dLat = degreesToRadians(lat2 - lat1)
    dLon = degreesToRadians(lon2 - lon1)

    lat1 = degreesToRadians(lat1)
    lat2 = degreesToRadians(lat2)

    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2)
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    distance = earthRadiusKm * c
    p 'haversine'
    p distance.to_s
    distance
  end

  def degreesToRadians(degrees)
    radians = degrees * (Math::PI / 180)
    radians
  end

  def notifyNeighbours(offer)
    users = User.where.not(id: @current_user.id)
    users.each do |the_user|
      result = @geocoder.geocode(the_user.address).first
      lat = result.coordinates[0]
      lng = result.coordinates[1]
      if haversineDistance(lat, lng, offer.latitude, offer.longitude) < 1
        UserMailer.notifyOfferCreated(the_user,offer).deliver_later
      end
    end
  end
end
