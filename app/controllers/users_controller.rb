# frozen_string_literal: true

class UsersController < ApplicationController

  def home; end

  def login; end

  def return
    redirect_to session.delete(:return_to)
  end

  def profile
    @user = User.find(params[:id])
    if @user
      @nbOffers = Offer.where(user_id: @user.id).count
    else
      flash[:info] = 'Cet utilisateur n\'existe pas!'
      redirect_to '/'
    end
  end

  def logout
    session[:user_id] = nil
    flash[:info] = 'Vous êtes maintenant déconnecté !'
    redirect_to '/'
  end

  def blockUser
    blocked = Blocked.new(blocked_by: params[:userBlocking], blocked_user: params[:userToBlock])
    if blocked.save
      flash[:info] = 'Utilisateur bloqué'
      redirect_to '/deliveries/myOffers'
    else
      flash[:info] = "Impossible de bloquer l'utilisateur"
      redirect_to '/deliveries/myOffers'
    end
  end

  def unBlockUser
    blocked = Blocked.where(blocked_by: params[:userBlocking], blocked_user: params[:userToBlock]).first
    if blocked
      blocked.destroy
      flash[:info] = 'Utilisateur débloqué'
      redirect_to '/deliveries/contact/' + params[:chatId]
    else
      flash[:info] = "L'utilisateur n'était pas bloqué"
      redirect_to '/deliveries/contact/' + params[:chatId]
    end
  end

  def checkLogin
    # @current_user = User.where(mail: params[:mail], password: params[:password]).first

    user = User.where(mail: params[:mail]).first
    if user
      hashedPassword = BCrypt::Engine.hash_secret(params[:password], user.salt)
      @current_user = user if user.password == hashedPassword
      if @current_user
        session[:user_id] = @current_user.id
        flash[:info] = 'Vous êtes maintenant connecté !'
        if session[:redirected_to] == 'createOffer'
          session.delete(:redirected_to)
          redirect_to '/deliveries/create'
        elsif session[:redirected_to] == 'contact'
          id = session[:chatId]
          session.delete(:chatId)
          session.delete(:redirected_to)
          redirect_to "/deliveries/createChat/#{id}"
        else
          redirect_to '/'
        end
      else
        session[:user_id] = nil
        flash[:info] = 'Échec de la connexion'
        redirect_to '/users/login'
      end
    else
      session[:user_id] = nil
      flash[:info] = 'Échec de la connexion'
      redirect_to '/users/login'
    end
  end

  def create
    if params[:password] != params[:passwordCheck]
      flash[:info] = 'Les mots de passe doivent être identiques'
      redirect_to '/users/signup'
    else
      salt = BCrypt::Engine.generate_salt
      submitted_password = params[:password]
      encrypted_password = BCrypt::Engine.hash_secret(submitted_password, salt)
      address = params[:address] || ''
      @user = User.new(name: params[:name], mail: params[:mail], password: encrypted_password, salt: salt, image: 'https://thispersondoesnotexist.com/image', register: DateTime.now, address: address)
      if @user.save
        session[:user_id] = @user.id
        flash[:info] = 'Inscription réussie, vous êtes maintenant connecté !'
        redirect_to '/'
      else
        flash[:info] = "Échec de l'inscription"
        redirect_to '/users/signup'
      end
    end
  end

end
