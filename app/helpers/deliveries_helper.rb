# frozen_string_literal: true

module DeliveriesHelper
  # helper pour afficher une offre
  def offer(the_offer)
    html = "<div class='container'><div class='row'><div class='col-sm-9 col-md-7 col-lg-5 mx-auto'><div class='card card-signin my-5'><div class='card-body'>".html_safe
    if the_offer.user_id
      html += '<b>Offre de: </b>'.html_safe
      html += User.find(the_offer.user_id).name
      html += "&nbsp;<a href='/users/profile/#{the_offer.user_id}'><i class='fas fa-address-card'>Voir le profil</i></a>".html_safe
      html += '<br>'.html_safe
    end

    if the_offer.end
      html += '<b>Livraison prévu: </b>'.html_safe + the_offer.end.strftime('%d/%m/%y à %H:%M').to_s
      html += '<br>'.html_safe
    end

    if the_offer.address
      html += "<b>Adresse de départ: </b><a onclick='toggle_visibility(#{the_offer.latitude},#{the_offer.longitude}); return false;' href='#'><i class='fas fa-map-marked-alt'>Voir sur la carte</i></a>".html_safe
      html += '<br>'.html_safe
      html += the_offer.address
      html += '<br>'.html_safe
    end

    if the_offer.description
      html += '<b>De la place pour des: </b>'.html_safe
      html += the_offer.description
      html += '<br>'.html_safe
    end

    html += "<div class='text-center'><br>".html_safe
    html += "<a class ='btn btn-primary' href='/deliveries/createChat/#{the_offer.id}'>".html_safe
    html += 'Contacter le voisin'
    html += '</a>'.html_safe
    html += '</div>'.html_safe

    html += '</div></div></div></div></div>'.html_safe
  end

  def message(the_message)
    user = User.find(the_message.sender_id)

    html = "<div class='chatUser'>".html_safe
    html += user.name
    html += ': '.html_safe
    html += '</div><div>&nbsp;'.html_safe
    html += the_message.content
    html += "</div><div class='chatTime'>&nbsp;&nbsp;".html_safe
    html += ' (envoyé à '
    html += '&nbsp;'.html_safe
    html += the_message.timestamp.to_s + ')'
    html += '</div>'.html_safe
  end

  def chat(the_chat)
    html = ''

    if @current_user.id == the_chat.user_id
      otherUserId = Offer.where(id: the_chat.offer_id).first.user_id
      chattingWith = User.find(Offer.find(the_chat.offer_id).user_id).name
    else
      otherUserId = the_chat.user_id
      chattingWith = User.find(the_chat.user_id).name
    end

    html += "<div class='row'>".html_safe
    if hasBeenBlocked(@current_user.id, otherUserId)
      html += 'Chat avec: ' + chattingWith
      html += "<p style='color:red'> (La livraison a été annulée!)</p>".html_safe
    elsif hasBlocked(@current_user.id, otherUserId)
      html += 'Chat avec: ' + chattingWith
      html += "<p style='color:red'> (Vous avez bloqué cet utilisateur!)</p>".html_safe
    else
      html += "<a href='/deliveries/contact/#{the_chat.id}'>".html_safe
      html += 'Chat avec: ' + chattingWith
      html += '</a>'.html_safe
    end
    html += '</div>'.html_safe

    html = html.html_safe
  end

  def mapClose(_cords)
    html = "<a  href='#' onclick='toggle_visibility(); return false;' id='link' ><h3><i class='fas fa-arrow-alt-circle-left'>Fermer la carte</i></h3></a>".html_safe
  end

  def isExpired(offer)
    return true if offer.start < DateTime.now

    false
  end

  def hasBeenBlocked(currentUserId, otherUserId)
    Blocked.where(blocked_by: otherUserId, blocked_user: currentUserId).exists?
  end

  def hasBlocked(currentUserId, otherUserId)
    Blocked.where(blocked_by: currentUserId, blocked_user: otherUserId).exists?
  end
end
