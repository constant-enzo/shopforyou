# frozen_string_literal: true

module UsersHelper
  def block(blocker, blocked,chatId)
    if @blocked
      html = "<a href='/users/unblock/#{blocker.id}/#{blocked.id}/#{chatId}' ><i class='fas fa-ban'>Débloquer l'utilisateur</i></a>".html_safe
    else
      html = "<a href='/users/block/#{blocker.id}/#{blocked.id}'  data-confirm='Voulez vous vraiment bloquer cet utilisateur?'><i class='fas fa-ban'>Bloquer l'utilisateur</i></a>".html_safe
    end
  end
end
