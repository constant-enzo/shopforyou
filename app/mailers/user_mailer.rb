# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def notifyOfferCreated(user, offer)
    @user = user
    @offer = offer
    mail(to: @user.mail, subject: 'Une nouvelle offre proche de chez vous!')
  end
end
