# frozen_string_literal: true

class Blocked < ActiveRecord::Base
  has_many :user
end
