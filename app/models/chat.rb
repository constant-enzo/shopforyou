class Chat < ActiveRecord::Base
    belongs_to :user
    has_many :message
    belongs_to :offer
end