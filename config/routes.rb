# frozen_string_literal: true

Rails.application.routes.draw do
  get 'deliveries/create'
  get 'deliveries/search' => 'deliveries#search'
  get 'deliveries/search/:adress' => 'deliveries#search'
  get 'deliveries/contact/:id' => 'deliveries#contact'
  get 'deliveries/createChat/:id' => 'deliveries#createChat'
  get 'deliveries/myOffers' => 'deliveries#myOffers'
  post 'deliveries/create' => 'deliveries#checkCreate'
  post 'deliveries/sendMessage/:chatId' => 'deliveries#sendMessage'
  root 'users#home'
  get 'users/home'
  get 'users/block/:userBlocking/:userToBlock' => 'users#blockUser'
  get 'users/unblock/:userBlocking/:userToBlock/:chatId' => 'users#unBlockUser'
  get 'users/login' => 'users#login'
  delete 'users/login' => 'users#logout'
  get 'users/logout' => 'users#logout'
  get 'users/return' => 'users#return'
  get 'users/signup' => 'users#signup'
  get 'users/profile/:id' => 'users#profile'
  post 'users/login' => 'users#checkLogin'
  post 'users/signup' => 'users#create'

  # post 'users/signup' => 'users#checkSignUp'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
