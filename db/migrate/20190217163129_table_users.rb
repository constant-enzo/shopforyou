class TableUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users
    add_column :users, :name, :string
    add_column :users, :mail, :string, unique: true
    add_column :users, :password, :string
    add_column :users, :image, :string
  end
end
