class Offer < ActiveRecord::Migration[5.2]
  def change
    create_table :offers
    add_column :offers, :start, :time
    add_column :offers, :end, :time
    add_column :offers, :address, :string
    add_column :offers, :description, :string
    add_column :offers, :user_id,:int
  end
end
