class AddIndexToUser < ActiveRecord::Migration[5.2]
  def change
    add_index :offers, :user_id
  end
end
