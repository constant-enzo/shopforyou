class AddGeoColumns < ActiveRecord::Migration[5.2]
  def change
    add_column :offers, :latitude, :decimal
    add_column :offers, :longitude, :decimal
  end
end
