class UpdateGeoColumns < ActiveRecord::Migration[5.2]
  def change
    change_column :offers, :latitude, :decimal, :precision => 10
    change_column :offers, :longitude, :decimal, :precision => 10
  end
end
