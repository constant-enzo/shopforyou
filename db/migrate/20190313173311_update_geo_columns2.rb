class UpdateGeoColumns2 < ActiveRecord::Migration[5.2]
  def change
    change_column :offers, :latitude, :decimal, :precision => 10, :scale=>5
    change_column :offers, :longitude, :decimal, :precision => 10,:scale=>5
  end
end
