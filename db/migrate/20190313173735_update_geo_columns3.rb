class UpdateGeoColumns3 < ActiveRecord::Migration[5.2]
  def change
    change_column :offers, :latitude, :decimal, :precision => 10,:scale=>7
    change_column :offers, :longitude, :decimal, :precision => 10,:scale=>7
  end
end
