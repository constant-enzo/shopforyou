class TableMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages
    add_column :messages, :content, :string
    add_column :messages, :timestamp, :time
    add_column :messages, :user_id, :int
    add_column :messages, :offer_id, :int
  end
end
