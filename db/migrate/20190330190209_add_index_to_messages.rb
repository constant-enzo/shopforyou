class AddIndexToMessages < ActiveRecord::Migration[5.2]
  def change
    add_index :messages, :user_id
    add_index :messages, :offer_id
  end
end
