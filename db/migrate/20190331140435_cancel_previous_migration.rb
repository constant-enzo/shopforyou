class CancelPreviousMigration < ActiveRecord::Migration[5.2]
  def change
    remove_column :messages, :offer_id
    add_column :messages, :chat_id, :int
  end
end
