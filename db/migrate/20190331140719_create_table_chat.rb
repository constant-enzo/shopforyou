class CreateTableChat < ActiveRecord::Migration[5.2]
  def change
    create_table :chats
    add_column :chats, :offer_id, :int
    add_column :chats, :user_id, :int
  end
end
