# frozen_string_literal: true

class ChangeOfferDateType < ActiveRecord::Migration[5.2]
  def change
    change_column :offers, :start, :datetime
    change_column :offers, :end, :datetime
  end
end
