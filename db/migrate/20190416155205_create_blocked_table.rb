class CreateBlockedTable < ActiveRecord::Migration[5.2]
  def change
    create_table :blockeds
    add_column :blockeds, :blocked_by, :int
    add_column :blockeds, :blocked_user, :int
    add_index :blockeds, :blocked_by
    add_index :blockeds, :blocked_user
    
  end
end
